import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Ng2AutoBreadCrumb} from "ng2-auto-breadcrumb";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FullLayoutComponent } from './layout/full-layout.component';

@NgModule({
  declarations: [
    AppComponent,
    FullLayoutComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    Ng2AutoBreadCrumb

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
